'use strict';

$(document).ready(function () {

    // Importing formHelpers module
    const formHelpers = require('base/checkout/formErrors');

    // Selecting elements
    const notifyMeRegisteredUserButton = ".js-capture-registered-user-btn";
    const customerPopup = ".js-customer-popup";
    const unregisteredUserForm = $("#unRegisteredUserForm");

    // Event handler for registered user button click
    $('body').on("click", notifyMeRegisteredUserButton ,function () {
        const actionURL = $(this).data("action");
        const productId = $(this).data("pid");
        // Creating data object to capture
        const dataToCapture = { productId: productId };
        // Instantiating NotifyMe object
        const notifyMe = new NotifyMe();
        // Invoking captureEmail method
        notifyMe.captureEmail(dataToCapture, actionURL);
        $(this).prop('disabled',true);
    });

    unregisteredUserForm.on("submit", function(e){
       e.preventDefault()        
       let formInputs  = $(this).serializeArray();
       // Creating data object to capture
       const dataToCapture = { productId: formInputs[1].value, email: formInputs[0].value };
       // Instantiating NotifyMe object
       const notifyMe = new NotifyMe();
       // Invoking captureEmail method
       notifyMe.captureEmail(dataToCapture, $(this)[0].action);
      
    })

    // Event handler for customer popup click
    $('body').on("click", customerPopup ,function () {

        let productID = $(this).data('pid');
        $("#backInStockCapturingProductID").val(productID);
        // Clearing previous errors
        formHelpers.clearPreviousErrors(unregisteredUserForm);
    });

    // NotifyMe constructor function
    const NotifyMe = function () {
        // Method to capture email
        this.captureEmail = function (dataToCapture, URL) {
            // Making AJAX request
            $.ajax({
                url: URL,
                type: 'post',
                dataType: 'json',
                data: dataToCapture,
                success: function (data) {
                    showAlertMessage(data.message,"OK");
                    closeCustomerPopUpModel();
                },
                error: function (err) {
                    console.log(err);
                    showAlertMessage("Something went wrong!","error");
                    closeCustomerPopUpModel();
                }
            });
        }

        let showAlertMessage = function (message, type) {
            var alertMessage = $(".alert-back-in-stock");
            alertMessage.html(message);
            alertMessage.show();
            if (type === "error")
                alertMessage.css("background-color", "red")
            else {
                alertMessage.css("background-color", "green")
            }
            // Hide the alert after 5 seconds (5000 milliseconds)
            setTimeout(function () {
                alertMessage.hide();
            }, 5000);
        }

        let closeCustomerPopUpModel = function(){
            $('#customerPopupModal').modal('hide');
            $('.back-in-stock-email').val('');
        }
    }

});