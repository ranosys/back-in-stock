'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var urlHelper = require('*/cartridge/scripts/helpers/urlHelpers');
var ImageModel = require('*/cartridge/models/product/productImages');

/**
 * Determines whether a product attribute has image swatches.  Currently, the only attribute that
 *     does is Color.
 * @param {string} dwAttributeId - Id of the attribute to check
 * @returns {boolean} flag that specifies if the current attribute should be displayed as a swatch
 */
function isSwatchable(dwAttributeId) {
    var imageableAttrs = ['color'];
    return imageableAttrs.indexOf(dwAttributeId) > -1;
}

/**
 * Retrieve all attribute values
 *
 * @param {dw.catalog.ProductVariationModel} variationModel - A product's variation model
 * @param {dw.catalog.ProductVariationAttributeValue} selectedValue - Selected attribute value
 * @param {dw.catalog.ProductVariationAttribute} attr - Attribute value'
 * @param {string} endPoint - The end point to use in the Product Controller
 * @param {string} selectedOptionsQueryParams - Selected options query params
 * @param {string} quantity - Quantity selected
 * @param {Object} inventoryList - inventory list
 * @returns {Object[]} - List of attribute value objects for template context
 */
function getAllAttrValues(
    variationModel,
    selectedValue,
    attr,
    endPoint,
    selectedOptionsQueryParams,
    quantity,
    inventoryList
) {
    var attrValues = variationModel.getAllValues(attr);
    var actionEndpoint = 'Product-' + endPoint;
    var Site = require('dw/system/Site');
    return collections.map(attrValues, function (value) {
        var isSelected = (selectedValue && selectedValue.equals(value)) || false;
        var valueUrl = '';

        var isVarientAvailable = false;

        if (Site.current.getCustomPreferenceValue('BackInStockNotifications_Enabled') && inventoryList) {
            var HashMap = require('dw/util/HashMap');
            var canBeSelected = variationModel.hasOrderableVariants(attr, value);
            var filter;
            var selectedAttributeValue;
            var allAttributes;
            if (!selectedValue) {
                if (!canBeSelected) {
                    filter = new HashMap();
                    allAttributes = variationModel.productVariationAttributes;
                    filter.put(attr.ID, value.value);
                    collections.forEach(allAttributes, function (selAttr) {
                        var selectedAttrValue = variationModel.getSelectedValue(selAttr);
                        if (selectedAttrValue) {
                            filter.put(selAttr.ID, selectedAttrValue.value);
                        }
                    });
                    selectedAttributeValue = variationModel.getVariants(filter);
                    if (selectedAttributeValue.length === 1) {
                        isVarientAvailable = true;
                    }
                } else {
                    filter = new HashMap();
                    filter.put(attr.ID, value.value);
                    selectedAttributeValue = variationModel.getVariants(filter);
                    if (selectedAttributeValue.length >= 1) {
                        isVarientAvailable = true;
                    }
                }
            } else if (!canBeSelected) {
                filter = new HashMap();
                allAttributes = variationModel.productVariationAttributes;
                filter.put(attr.ID, value.value);
                collections.forEach(allAttributes, function (selAttr) {
                    var selectedAttrValue = variationModel.getSelectedValue(selAttr);
                    if (selectedAttrValue) {
                        if (attr.ID !== selAttr.ID) {
                            filter.put(selAttr.ID, selectedAttrValue.value);
                        }
                    }
                });
                selectedAttributeValue = variationModel.getVariants(filter);
                if (selectedAttributeValue.length === 1) {
                    isVarientAvailable = true;
                }
            } else {
                isVarientAvailable = true;
            }
        } else {
            isVarientAvailable = variationModel.hasOrderableVariants(attr, value);
        }

        var processedAttr = {
            id: value.ID,
            description: value.description,
            displayValue: value.displayValue,
            value: value.value,
            selected: isSelected,
            selectable: isVarientAvailable
        };

        if (processedAttr.selectable) {
            valueUrl = (isSelected && endPoint !== 'Show')
                ? variationModel.urlUnselectVariationValue(actionEndpoint, attr)
                : variationModel.urlSelectVariationValue(actionEndpoint, attr, value);
            processedAttr.url = urlHelper.appendQueryParams(valueUrl, [selectedOptionsQueryParams,
                'quantity=' + quantity]);
        }

        if (isSwatchable(attr.attributeID)) {
            processedAttr.images = new ImageModel(value, { types: ['swatch'], quantity: 'all' });
        }

        return processedAttr;
    });
}

/**
 * Gets the Url needed to relax the given attribute selection, this will not return
 * anything for attributes represented as swatches.
 *
 * @param {Array} values - Attribute values
 * @param {string} attrID - id of the attribute
 * @returns {string} -the Url that will remove the selected attribute.
 */
function getAttrResetUrl(values, attrID) {
    var urlReturned;
    var value;

    for (var i = 0; i < values.length; i++) {
        value = values[i];
        if (!value.images) {
            if (value.selected) {
                urlReturned = value.url;
                break;
            }

            if (value.selectable) {
                urlReturned = value.url.replace(attrID + '=' + value.value, attrID + '=');
                break;
            }
        }
    }

    return urlReturned;
}

/**
 * @constructor
 * @classdesc Get a list of available attributes that matches provided config
 *
 * @param {dw.catalog.ProductVariationModel} variationModel - current product variation
 * @param {Object} attrConfig - attributes to select
 * @param {Array} attrConfig.attributes - an array of strings,representing the
 *                                        id's of product attributes.
 * @param {string} attrConfig.attributes - If this is a string and equal to '*' it signifies
 *                                         that all attributes should be returned.
 *                                         If the string is 'selected', then this is comming
 *                                         from something like a product line item, in that
 *                                         all the attributes have been selected.
 *
 * @param {string} attrConfig.endPoint - the endpoint to use when generating urls for
 *                                       product attributes
 * @param {string} selectedOptionsQueryParams - Selected options query params
 * @param {string} quantity - Quantity selected
 */
function VariationAttributesModel(variationModel, attrConfig, selectedOptionsQueryParams, quantity) {
    var allAttributes = variationModel.productVariationAttributes;
    var result = [];
    var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
    var inventoryList = ProductInventoryMgr.getInventoryList();

    collections.forEach(allAttributes, function (attr) {
        var selectedValue = variationModel.getSelectedValue(attr);
        var values = getAllAttrValues(
            variationModel,
            selectedValue,
            attr,
            attrConfig.endPoint,
            selectedOptionsQueryParams,
            quantity,
            inventoryList
        );
        var resetUrl = getAttrResetUrl(values, attr.ID);

        if ((Array.isArray(attrConfig.attributes)
            && attrConfig.attributes.indexOf(attr.attributeID) > -1)
            || attrConfig.attributes === '*') {
            result.push({
                attributeId: attr.attributeID,
                displayName: attr.displayName,
                id: attr.ID,
                swatchable: isSwatchable(attr.attributeID),
                displayValue: selectedValue && selectedValue.displayValue ? selectedValue.displayValue : '',
                values: values,
                resetUrl: resetUrl
            });
        } else if (attrConfig.attributes === 'selected') {
            result.push({
                displayName: attr.displayName,
                displayValue: selectedValue && selectedValue.displayValue ? selectedValue.displayValue : '',
                attributeId: attr.attributeID,
                id: attr.ID
            });
        }
    });

    // var result = optBackInStockNotifyMe(result, selectedArrtWithValues, variationModel);
    result.forEach(function (item) {
        this.push(item);
    }, this);
}

VariationAttributesModel.prototype = [];

module.exports = VariationAttributesModel;
