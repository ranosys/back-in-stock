'use strict';

var Site = require('dw/system/Site');
var Logger = require('dw/system/Logger').getLogger('BackInStock', 'BackInStock');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');

/**
 * Sends email to admin notifying the low stoc products
 * @param {Object} params - job Configurations
 * @param {Object} capturedData lowStockNotification for which email needs to be sent
 */
function sendMail(params, capturedData) {
    var HashMap = require('dw/util/HashMap');
    var Mail = require('dw/net/Mail');
    var Template = require('dw/util/Template');

    try {
        var context = new HashMap();
        context.put('capturedData', capturedData);
        var notificationEmail = new Mail();
        notificationEmail.addTo(params.AdminEmail.split(','));
        notificationEmail.setFrom(params.FromEmailAddress);
        notificationEmail.setSubject(params.EmailSubject);
        var template = new Template('lowStockNotification/notificationToAdmin');
        var content = template.render(context).text;
        notificationEmail.setContent(content, 'text/html', 'UTF-8');
        notificationEmail.send();
        Logger.info('Low Stock Notification email sent');
    } catch (exception) {
        Logger.error('Unable to send email:' + exception);
    }
}

/**
 * Cleans up Custom Object data for lowStockNotification once email is sent
 * @param {Object} capturedData lowStockNotification for which email is sent
 */
function cleanupCustomObjects(capturedData) {
    var Transaction = require('dw/system/Transaction');

    try {
        for (var i = 0; i < capturedData.length; i++) {
            var productID = capturedData[i].productID;
            var productRecord = CustomObjectMgr.getCustomObject('lowStockNotification', productID);
            if (productRecord) {
                // Remove existing custom object
                Transaction.wrap(function () {
                    CustomObjectMgr.remove(productRecord);
                });
                Logger.info('Custom Object deleted. Product ID: ' + productID);
            }
        }
    } catch (exception) {
        Logger.error('Unable to delete Custom Object:' + exception);
    }
}

/**
 * Exports lowStockNotification data to CSV
 * @param {Object} params - job Configurations
 * @param {Object} data lowStockNotification for which email is sent
 */
function exportToCsv(params, data) {
    // Import required modules
    var FileWriter = require('dw/io/FileWriter');
    var CSVStreamWriter = require('dw/io/CSVStreamWriter');
    var File = require('dw/io/File');
    var csvHelper = require('~/cartridge/scripts/helpers/generateCSVHelpers.js');

    // Create the main directory for the CSV file
    var mainDir = new File(File.IMPEX + params.BackUpLocation + Site.getCurrent().getName());

    // Generate the import file name and prepare the import file
    var filename = csvHelper.getLowStockFileName();
    var importFile = csvHelper.prepareImportFile(mainDir, filename);

    // Initialize FileWriter and CSVStreamWriter
    var fileWriter = new FileWriter(importFile);
    var csvWriter = new CSVStreamWriter(fileWriter);

    // Write CSV headers
    var headers = ['S_No', 'ProductID', 'Product_Name', 'ATS'];
    csvWriter.writeNext(headers);

    // Write data to CSV file
    for (var i = 0; i < data.length; i++) {
        var values = [
            i + 1,
            data[i].productID,
            data[i].productName,
            data[i].ATS
        ];
        csvWriter.writeNext(values);
    }
    csvWriter.close();
}

/**
 * Processes data captured in custom object lowStockNotification
 * @param {Object} parameter - job Configurations
 * @returns {boolean} true when job processing is done
 */
function processCapturedData(parameter) {
    // Import required modules
    var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
    var LowInventoryThreshold = Site.current.getCustomPreferenceValue('LowInventoryNotification_Threshold');
    var notificationEnabled = Site.current.getCustomPreferenceValue('LowInventoryNotification_Enabled');

    if (notificationEnabled) {
        // Query lowStockNotification custom objects
        var capturedData = CustomObjectMgr.getAllCustomObjects('lowStockNotification').asList();
        // Retrieve product inventory list
        var inventoryList = ProductInventoryMgr.getInventoryList();
        // an array to store product IDs with stock below  threshold
        var capturedDatahavingLowInventory = [];
        var capturedDatahavingInventory = []; //--

        if (inventoryList && capturedData) {
            // Loop through each back-in-stock custom object
            for (var i = 0; i < capturedData.length; i++) {
                var productID = capturedData[i].custom.ProductID;
                var productName = capturedData[i].custom.ProductName;
                // Retrieve inventory record for the corresponding product ID
                var inventoryRecord = inventoryList.getRecord(productID);
                // Check if inventory record exists and satisfies conditions
                if (inventoryRecord && inventoryRecord.ATS.value < LowInventoryThreshold) {
                    // Add product ID to the list
                    capturedDatahavingLowInventory.push({
                        ATS: inventoryRecord.ATS.value,
                        productName: productName,
                        productID: productID
                    });
                } else {
                    capturedDatahavingInventory.push({
                        ATS: inventoryRecord.ATS.value,
                        productName: productName,
                        productID: productID
                    });
                }
            }
        }

        if (capturedDatahavingInventory.length > 0) {
            // Delete custom objects with inventory
            cleanupCustomObjects(capturedDatahavingInventory);
        }
        if (capturedDatahavingLowInventory.length > 0) {
            // Send Email to admin
            sendMail(parameter, capturedDatahavingLowInventory);
            // Export custom objects to csv for back up before deleting
            exportToCsv(parameter, capturedDatahavingLowInventory);
            // Delete custom objects with low inventory after email is sent
            cleanupCustomObjects(capturedDatahavingLowInventory);
        }
    }
    return true;
}

exports.processCapturedData = processCapturedData;
