'use strict';
var Site = require('dw/system/Site');
var Logger = require('dw/system/Logger');


/**
 * Processes captured data to identify customers interested in restocking products.
 * @returns {boolean} - Returns false indicating completion of processing.
 */
function processCapturedData() {
    // Import required modules
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Constants = require('*/cartridge/scripts/utils/constants');
    var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
    var logger = Logger.getLogger('BackInStock', 'BackInStock');
    var Site = require('dw/system/Site');
    var productMgr = require('dw/catalog/ProductMgr')
    var Transaction = require('dw/system/Transaction');

    // Retrieve the back-in-stock threshold value from site preferences
    var backInStockThresholdValue = Site.current.getCustomPreferenceValue('BackInStockNotifications_Threshold');  
    logger.info('Threshold :' + backInStockThresholdValue);
    // Query back-in-stock custom objects
    var capturedData = CustomObjectMgr.queryCustomObjects(
        Constants.BackInStock_CONFIGURATION_OBJECT,
        'custom.IsMailSent = false', // Filter to get unsent notifications
        'creationDate desc', // Sort by creation date in descending order
        null
    ).asList();

    // Retrieve product inventory list
    var inventoryList = ProductInventoryMgr.getInventoryList();
    var capturedDataWithInventory = []; // Initialize an array to store email IDs of customers interested in restocking

    // Check if both inventory list and back-in-stock objects exist
    if (inventoryList && capturedData) {
        // Loop through each back-in-stock custom object
        for (var i = 0; i < capturedData.length; i++) {
            var productID = capturedData[i].custom.ProductID;
            var emailIdList = capturedData[i].custom.EmailID;

            // Retrieve product and inventory record for the corresponding product ID
            var product = productMgr.getProduct(productID);
            var inventoryRecord = product.availabilityModel.inventoryRecord;

            // Check if inventory record exists and satisfies conditions
            if (inventoryRecord && inventoryRecord.ATS.value >= backInStockThresholdValue) {
                // Add data to the array
                capturedDataWithInventory.push({
                    customerEmailIds: emailIdList,
                    productID: productID,
                    creationDate: capturedData[i].creationDate,
                    modifiedDate: capturedData[i].lastModified,
                    productName: product.name,
                    productShortDescription: product.shortDescription.source,
                    productImage: product.variationModel.getImage('medium').getURL().abs().toString(),
                    productPageURL_MasterProductID: product.master ? productID : product.masterProduct.ID
                });

                var backInStockData = CustomObjectMgr.getCustomObject(Constants.BackInStock_CONFIGURATION_OBJECT, productID)
                Transaction.wrap(function () {
                    backInStockData.custom.IsMailSent = true;
                });

            }
        }

        try {
            //mail send to users
            sendMail(capturedDataWithInventory);
        }
        catch (exception) {
            logger.error('Error sending mails to customers : ' + exception);
        }
    }

    return true;
}

/**
 * Maps customers with products they are interested in.
 * @param {Array<Object>} capturedDataWithInventory - Array containing captured data with inventory details.
 * @returns {Object} - Returns a map of customers with associated products.
 */
function mapCustomersWithProducts(capturedDataWithInventory) {
    var customerMap = {};
    var URLUtils = require('dw/web/URLUtils');
    var URLAction = require('dw/web/URLAction');
    var URLParameter = require('dw/web/URLParameter');

    // Iterate over each object in the data array
    capturedDataWithInventory.forEach(item => {
        // Iterate over each customer in the 'customerEmailIds' array of the current object
        item.customerEmailIds.forEach(customer => {
            var productDetails = {
                productID: item.productID,
                productName: item.productName,
                productImage: item.productImage,
                productShortDescription: item.productShortDescription,
                productPageURL: URLUtils.abs(new URLAction('Product-Show', Site.getCurrent().name, JSON.parse(customer).language), new URLParameter('pid', item.productPageURL_MasterProductID))
            };
            // Check if the customer already exists in the customerMap
            if (Object.prototype.hasOwnProperty.call(customerMap,customer)) {
                // Add the product to the existing customer entry
                customerMap[customer].productDetails.push(productDetails);
            } else {
                // Create a new entry for the customer with the associated product              
                customerMap[customer] = { customer: [customer], productDetails: [productDetails] };
            }
        });
    });

    return customerMap;
}

/**
 * Sends notification emails to customers interested in restocking.
 * @param {Array<Object>} capturedDataWithInventory - Array containing captured data with inventory details.
 */
function sendMail(capturedDataWithInventory) {
    var Site = require('dw/system/Site');
    var Resource = require('dw/web/Resource');
    var HashMap = require('dw/util/HashMap');
    var Mail = require('dw/net/Mail');
    var Template = require('dw/util/Template');
    
    var logger = Logger.getLogger('BackInStock', 'BackInStock');
    var fromEmailIds;

    // Map customers with products
    var customerSelectedProducts = mapCustomersWithProducts(capturedDataWithInventory);
    

    try {
        // Retrieve the 'from' email IDs from site preferences
        fromEmailIds = Site.current.getCustomPreferenceValue('BackInStockNotifications_FromEmail');

        // Set 'from' email IDs for the notification email
        if (!fromEmailIds) {
            fromEmailIds ='no-reply@salesforce.com';            
        }
    } catch (exception) {
        logger.error('Error binding BackInStockNotifications_FromEmail : ' + exception);
        fromEmailIds ='no-reply@salesforce.com';        
    }   

    // Get the keys (customer email IDs) from the customerSelectedProducts map
    var dataKeys = Object.keys(customerSelectedProducts);

    // Loop through each customer and associated products
    for (var i = 0; i < dataKeys.length; i++) {

        var notificationEmail = new Mail();
        var context = new HashMap();
        // Set email subject

        var emailSubject = Site.current.getCustomPreferenceValue('BackInStockNotifications_EmailSubject');

        var JSONEmailSubject;
        try {
            JSONEmailSubject = JSON.parse(emailSubject)[JSON.parse(dataKeys[i]).language];
            if (!JSONEmailSubject) {
                JSONEmailSubject = JSON.parse(emailSubject)['default'];
                if (!JSONEmailSubject) {
                    JSONEmailSubject = Resource.msg('back.in.stock.email.notification.subject', 'common', null);
                }
            }
        }
        catch (exception) {
            logger.error('Error binding BackInStockNotifications_EmailSubject : ' + exception);
            JSONEmailSubject = Resource.msg('back.in.stock.email.notification.subject', 'common', null);
        }
        notificationEmail.setSubject(JSONEmailSubject);
        notificationEmail.setFrom(fromEmailIds);
        // Set email recipient
        notificationEmail.addTo(JSON.parse(dataKeys[i]).email);

        // Set email content
        context.productDetails = customerSelectedProducts[dataKeys[i]].productDetails;

        var template = new Template('backInStock/notification/notificationToCustomers');
        var content = template.render(context).text;
        notificationEmail.setContent(content, 'text/html', 'UTF-8');

        // Send email
        notificationEmail.send();
    }

    return true;
}

exports.processCapturedData = processCapturedData;