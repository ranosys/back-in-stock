'use strict';

var Site = require('dw/system/Site');
var Logger = require('dw/system/Logger').getLogger('BackInStock', 'BackInStock');

/**
 * Sends email to admin notifying the low stoc products
 * @param {Object} params - job Configurations
 * @param {Object} capturedData lowStockNotification for which email needs to be sent
 */
function sendMail(params, capturedData) {
    var HashMap = require('dw/util/HashMap');
    var Mail = require('dw/net/Mail');
    var Template = require('dw/util/Template');

    try {
        var context = new HashMap();
        context.put('capturedData', capturedData);
        var notificationEmail = new Mail();
        notificationEmail.addTo(params.AdminEmail.split(','));
        notificationEmail.setFrom(params.FromEmailAddress);
        notificationEmail.setSubject(params.EmailSubject);
        var template = new Template('lowStockNotification/notificationToAdmin');
        var content = template.render(context).text;
        notificationEmail.setContent(content, 'text/html', 'UTF-8');
        notificationEmail.send();
        Logger.info('Low Stock Notification email sent');
    } catch (exception) {
        Logger.error('Unable to send email:' + exception);
    }
}

/**
 * Exports lowStockNotification data to CSV
 * @param {Object} params - job Configurations
 * @param {Object} data lowStockNotification for which email is sent
 */
function exportToCsv(params, data) {
    // Import required modules
    var FileWriter = require('dw/io/FileWriter');
    var CSVStreamWriter = require('dw/io/CSVStreamWriter');
    var File = require('dw/io/File');
    var csvHelper = require('~/cartridge/scripts/helpers/generateCSVHelpers.js');

    // Create the main directory for the CSV file
    var mainDir = new File(File.IMPEX + params.BackUpLocation + Site.getCurrent().getName());

    // Generate the import file name and prepare the import file
    var filename = csvHelper.getLowStockFileName();
    var importFile = csvHelper.prepareImportFile(mainDir, filename);

    // Initialize FileWriter and CSVStreamWriter
    var fileWriter = new FileWriter(importFile);
    var csvWriter = new CSVStreamWriter(fileWriter);

    // Write CSV headers
    var headers = ['S_No', 'ProductID', 'Product_Name', 'ATS'];
    csvWriter.writeNext(headers);

    // Write data to CSV file
    for (var i = 0; i < data.length; i++) {
        var values = [
            i + 1,
            data[i].productID,
            data[i].productName,
            data[i].ATS
        ];
        csvWriter.writeNext(values);
    }
    csvWriter.close();
}

/**
 * Processes data captured in custom object lowStockNotification
 * @param {Object} parameter - job Configurations4
 * @returns {boolean} true when job processing is done
 */
function process(parameter) {
    // Import required modules
    var productMgr = require('dw/catalog/ProductMgr');
    var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
    var LowInventoryThreshold = Site.current.getCustomPreferenceValue('LowInventoryNotification_Threshold');
    // Retrieve product inventory list
    var inventoryList = ProductInventoryMgr.getInventoryList();
    // an array to store product IDs with stock below  threshold
    var capturedDatahavingLowInventory = [];
    var orderableProducts = [];

    try {
        var allProducts = productMgr.queryAllSiteProducts();
        var count = allProducts.getCount();
        Logger.info('==================Low Stock Notification Full==================');
        Logger.info('All Products Count: ' + count);
        while (allProducts.hasNext()) {
            var productHit = allProducts.next();
            //  Logger.info('1: productID: ' + productHit.ID + '- isMaster : ' + productHit.master + '- is available:' + productHit.available);
            var inventoryRecord = inventoryList.getRecord(productHit.ID);
            if (inventoryRecord && inventoryRecord.ATS.value < LowInventoryThreshold) {
                capturedDatahavingLowInventory.push({
                    ATS: inventoryRecord.ATS.value,
                    productName: productHit.name,
                    productID: productHit.ID
                });
                Logger.info('LowInventory: productID: ' + productHit.ID
                            + '- isMaster : ' + productHit.master
                            + '- is available:' + productHit.available
                            + 'assignedToSiteCatalog: ' + productHit.assignedToSiteCatalog
                            + '- bundle:' + productHit.bundle
                            + '- is bundled:' + productHit.bundled
                            + '- is categorized:' + productHit.categorized
                            + '- is productSet:' + productHit.productSet
                            + '- is productSetProduct:' + productHit.productSetProduct
                            + '- is siteProduct:' + productHit.siteProduct);
            }
            orderableProducts.push(productHit.ID);
        }
        if (capturedDatahavingLowInventory.length > 0) {
            // Send Email to admin
            sendMail(parameter, capturedDatahavingLowInventory);
            // Export custom objects to csv for back up before deleting
            exportToCsv(parameter, capturedDatahavingLowInventory);
        }
    } catch (e) {
        Logger.error('Unable to get low stock products:' + e.message + e.stack);
    }

    Logger.info('orderableProducts:' + orderableProducts.length);
    Logger.info('capturedDatahavingLowInventory:' + capturedDatahavingLowInventory.length);

    return true;
}

exports.process = process;
