'use strict';

/**
 * Clears the captured notification data from the custom object and generates a CSV file.
 *
 * @returns {boolean} Indicates whether the operation was successful.
 */
function clearCapturedNotificationData() {
    // Import required modules
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Constants = require('*/cartridge/scripts/utils/constants');
    var generateCSVHelpers = require('~/cartridge/scripts/helpers/generateCSVHelpers');
    var Transaction = require('dw/system/Transaction');

    // Query back-in-stock custom objects with sent notifications
    var backInStockData = CustomObjectMgr.queryCustomObjects(
        Constants.BackInStock_CONFIGURATION_OBJECT,
        'custom.IsMailSent = true', // Filter to get sent notifications
        'creationDate desc', // Sort by creation date in descending order
        null
    );

    // Check if back-in-stock data exists
    if (backInStockData) {
        var backInStockDataList = backInStockData.asList();

        // Generate CSV file from captured data
        generateCSVHelpers.createCSVfromCapturedData(backInStockDataList);

        // Remove back-in-stock data from the custom object
        Transaction.wrap(function () {
            for (var i = 0; i < backInStockDataList.length; i++) {
                CustomObjectMgr.remove(backInStockDataList[i]);
            }
        });
    }

    return true; // Operation completed successfully
}

// Export the function for external use
exports.clearCapturedNotificationData = clearCapturedNotificationData;
