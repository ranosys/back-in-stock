'use strict';

var File = require('dw/io/File');
var Site = require('dw/system/Site');
var Logger = require('dw/system/Logger');

/**
 * Prepares the CSV import file.
 *
 * @param {Object} importDir - The directory for the CSV import file.
 * @param {string} fileName - The name of the CSV import file.
 * @returns {Object} The prepared CSV import file.
 */
function prepareImportFile(importDir, fileName) {
    // var File = require('dw/io/File');
    var localeDir = new File(importDir.getFullPath());
    if (!localeDir.exists()) {
        localeDir.mkdirs();
    }
    var file = new File(localeDir.getFullPath() + File.SEPARATOR + fileName);
    return file;
}

/**
 * Generates the CSV import filename.
 *
 * @returns {string} The generated import file name.
 */
function getImportFileName() {
    // var Site = require('dw/system/Site');
    var StringUtils = require('dw/util/StringUtils');
    var Calendar = require('dw/util/Calendar');
    var logger = Logger.getLogger('BackInStock', 'BackInStock');
    var timestamp = StringUtils.formatCalendar(new Calendar(new Date(Site.getCalendar().time.toString())), 'yyyyMMdd-HHmmss');
    logger.info('BackInStock captured data file name : ' + StringUtils.format('sentNotificationData-{0}.csv', timestamp));
    return StringUtils.format('sentNotificationData-{0}.csv', timestamp);
}

/**
 * Creates a CSV file from the captured notification data.
 *
 * @param {Object[]} data - The captured notification data to be written to the CSV file.
 * @return {void}
 */
function createCSVfromCapturedData(data) {
    // Import required modules
    var FileWriter = require('dw/io/FileWriter');
    var CSVStreamWriter = require('dw/io/CSVStreamWriter');
    var logger = Logger.getLogger('BackInStock', 'BackInStock');

    var reportLocation = Site.current.getCustomPreferenceValue('BackInStockNotifications_ReportLocation');
    if (!reportLocation) {
        logger.error('Please configure the export directory in the site preferences.');
        return false;
    }

    var dir = File.IMPEX + reportLocation;
    // Create the main directory for the CSV file
    var mainDir = new File(dir);

    logger.info('Directory to import CSV file : ' + dir);

    // Generate the import file name and prepare the import file
    var filename = getImportFileName();
    var importFile = prepareImportFile(mainDir, filename);

    // Initialize FileWriter and CSVStreamWriter
    var fileWriter = new FileWriter(importFile);
    var csvWriter = new CSVStreamWriter(fileWriter);

    // Write CSV headers
    var headers = ['S_No', 'Customer_Email', 'Product_Ids', 'Mail_Sent', 'Created_Date', 'Modified_Date'];
    csvWriter.writeNext(headers);

    // Write data to CSV file
    for (var i = 0; i < data.length; i++) {
        var values = [
            i + 1,
            data[i].custom.EmailID.join('&##'),
            data[i].custom.ProductID,
            data[i].custom.IsMailSent,
            data[i].creationDate,
            data[i].lastModified
        ];
        csvWriter.writeNext(values);
    }
    csvWriter.close();

    return true;
}

/**
 * Generates the CSV filename.
 *
 * @returns {string} The generated import file name.
 */
function getLowStockFileName() {
    var StringUtils = require('dw/util/StringUtils');
    var Calendar = require('dw/util/Calendar');
    var timestamp = StringUtils.formatCalendar(new Calendar(new Date(Site.getCalendar().time.toString())), 'yyyyMMdd-HHmmss');
    return StringUtils.format('lowStockNotificationData-{0}.csv', timestamp);
}

// Export the function for external use
exports.createCSVfromCapturedData = createCSVfromCapturedData;
exports.getLowStockFileName = getLowStockFileName;
exports.prepareImportFile = prepareImportFile;
