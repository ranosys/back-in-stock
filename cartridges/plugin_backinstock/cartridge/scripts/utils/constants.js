'use strict';

/**
 * @module scripts/utils/Contants
 *
 * This is a file used to put all constants here which will be reused in BackInStock cartridge.
 */

exports.BackInStock_CONFIGURATION_OBJECT = 'BackInStock';
