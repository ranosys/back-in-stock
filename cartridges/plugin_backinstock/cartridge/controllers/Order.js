/**
 * @namespace Order
 */

const server = require('server');

server.extend(module.superModule);

/**
  * Order-Confirm : This endpoint is invoked when the shopper's Order is Placed and Confirmed
  * @name Base/Order-Confirm
  * @function
  * @memberof Order
  */
server.append('Confirm', (req, res, next) => {
    // Import required modules
    const Site = require('dw/system/Site');
    const ProductMgr = require('dw/catalog/ProductMgr');
    const Transaction = require('dw/system/Transaction');
    const CustomObjectMgr = require('dw/object/CustomObjectMgr');
    const ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
    const Logger = require('dw/system/Logger').getLogger('BackInStock', 'BackInStock');
    var notificationEnabled = Site.current.getCustomPreferenceValue('LowInventoryNotification_Enabled');
    const viewData = res.getViewData();
    const orderModel = viewData.order;

    if (notificationEnabled) {
        const threshold = Site.current.getCustomPreferenceValue('LowInventoryNotification_Threshold');
        if (threshold) {
            for (let i = 0; i < orderModel.shipping.length; i++) {
                for (let j = 0; j < orderModel.shipping[i].productLineItems.items.length; j++) {
                    var items = orderModel.shipping[i].productLineItems.items;
                    var pid = items[j].id;
                    var product = ProductMgr.getProduct(pid);
                    var quantity = ProductInventoryMgr.getInventoryList().getRecord(product).getATS().getValue();
                    var productRecord = CustomObjectMgr.getCustomObject('lowStockNotification', product.ID);
                    if (productRecord) {
                        // Inventory below Threshold
                        if (quantity < threshold) {
                            Transaction.wrap(() => {
                                productRecord.custom.ATS = quantity;
                            });
                            Logger.info('Updated custom object. ProductID:' + product.ID + '& ATS:' + quantity );
                        }
                        // Inventory above Threshold
                        else {
                            Transaction.wrap(() => {
                                // Remove existing custom object
                                CustomObjectMgr.remove(productRecord);
                            });
                            Logger.info('Removed custom object. ProductID:' + product.ID + '& ATS:' + quantity );
                        }
                    } else {
                        try {
                            Transaction.wrap(() => {
                                let lowStockNotification = CustomObjectMgr.createCustomObject('lowStockNotification', product.ID);
                                lowStockNotification.custom.ATS = quantity;
                                lowStockNotification.custom.ProductName = product.name;
                            });
                            Logger.info('Created custom object. ProductID: ' + product.ID + ' & ATS: ' + quantity );
                        } catch (error) {
                            Logger.error('Unable to create custom object email: ' + error);
                        }
                    }
                }
            }
        }
    }
    res.setViewData(viewData);
    next();
});

module.exports = server.exports();
