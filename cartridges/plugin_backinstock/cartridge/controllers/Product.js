'use strict';

/**
 * @namespace Product
 */

var server = require('server');
server.extend(module.superModule);

/**
  * Product-Show : This endpoint is called to get the details of the current customer
  * @name Base/Product-Show
  * @function
  * @memberof Product     
  */
server.append('Show', function (req, res, next) {
  var AccountModel = require('*/cartridge/models/account');
  var customObjectMgr = require('dw/object/CustomObjectMgr');
  var Site = require('dw/system/Site');
  var constants = require('*/cartridge/scripts/utils/constants');
  var accountModel = new AccountModel(req.currentCustomer);
  let viewData = res.getViewData();
  
  viewData.enableBackInStockCartridge = Site.current.getCustomPreferenceValue('BackInStockNotifications_Enabled');
  viewData.notifyMeStatus = false;
  if(res.viewData.product){
    var backInStockData = customObjectMgr.getCustomObject(constants.BackInStock_CONFIGURATION_OBJECT, res.viewData.product.id);  
    if (accountModel.registeredUser && backInStockData) {
      viewData.notifyMeStatus = backInStockData.custom.EmailID.find(x => JSON.parse(x).email == accountModel.profile.email) != null ? true : false;
    }
  }

  viewData.customer = accountModel,
    next();
});

server.append('Variation', function (req, res, next) {
  var AccountModel = require('*/cartridge/models/account');
  var customObjectMgr = require('dw/object/CustomObjectMgr');
  var Site = require('dw/system/Site');
  var constants = require('*/cartridge/scripts/utils/constants')
  var Resource = require('dw/web/Resource');
  var URLUtils = require('dw/web/URLUtils');
  var productMgr = require('dw/catalog/ProductMgr');  
  var accountModel = new AccountModel(req.currentCustomer);
  var enableBackInStockCartridge = Site.current.getCustomPreferenceValue('BackInStockNotifications_Enabled');
  var isCustomerAuthenticated = accountModel.registeredUser;
  var productId = res.viewData.product.id;
  var notifyMeStatus = false;
  if(productId)
  {
    var product = productMgr.getProduct(productId);
    var productToCapture = product.master ? null : productId;
    if(productToCapture){
      var backInStockData = customObjectMgr.getCustomObject(constants.BackInStock_CONFIGURATION_OBJECT, productId); 
      if (isCustomerAuthenticated && backInStockData) {
          notifyMeStatus = backInStockData.custom.EmailID.find(x => JSON.parse(x).email == accountModel.profile.email) != null ? true : false;
      }
    }
  }

  res.json({   
    isNotifyMeCartridgeEnabled: enableBackInStockCartridge,
    isCustomerAuthenticated:isCustomerAuthenticated,
    IsProductAlreadySubscribed: notifyMeStatus,
    notifyMeBtnLbl: Resource.msg('button.notifyme', 'common', null),
    dataCaptureURL: URLUtils.url('BackInStock-CaptureData').toString()
  });

  next();
});


module.exports = server.exports();
