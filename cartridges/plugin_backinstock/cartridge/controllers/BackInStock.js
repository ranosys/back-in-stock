'use strict';

/**
 * @namespace BackInStock
 */

var server = require('server');
var Logger = require('dw/system/Logger');
/**
 * Main entry point for Checkout
 */

/**
 * Checkout-Begin : The Checkout-Begin endpoint will render the checkout shipping page for both guest shopper and returning shopper
 * @name Base/BackInStock-CaptureData
 * @function
 * @memberof BackInStock
 * @param {querystringparameter} - email :- gurest customer and product Id
 * @param {category} - sensitive
 * @param {serverfunction} - post
 */

server.post(
    'CaptureData', function (req, res, next) {
        var AccountModel = require('*/cartridge/models/account');
        var customObjectMgr = require('dw/object/CustomObjectMgr');
        var constants = require('*/cartridge/scripts/utils/constants');
        var Transaction = require('dw/system/Transaction');
        var Resource = require('dw/web/Resource');
        var productMgr = require('dw/catalog/ProductMgr')
        var Locale = require('dw/util/Locale');
        var logger = Logger.getLogger('BackInStock', 'BackInStock');
        var URLUtils = require('dw/web/URLUtils');
        var emailToCapture = req.httpParameterMap.get('email').stringValue;
        var productId = req.httpParameterMap.get('productId').stringValue;
        var accountModel = new AccountModel(req.currentCustomer);        

        var product = productMgr.getProduct(productId);

        if (!product || product.master) {
            logger.error('product not found ID :' + productId);
            res.json({
                message: Resource.msg('message.confirm.product.not.found.error', 'common', null)
            });
        }

        var productToCapture = product.ID;
        var currentLocale = Locale.getLocale(req.locale);

        if (!emailToCapture) {
            if (accountModel.registeredUser) {
                emailToCapture = accountModel.profile.email;
            }
            else {
                res.redirect(URLUtils.url('Login-Show'));
                logger.error('Unable to capture email id :' + emailToCapture);
                return next();
            }
        }

        var currentLocaleID;

        if (!currentLocale) {
            logger.info('No site locale mapping found');
            currentLocaleID = 'default';
        }
        else {
            currentLocaleID = currentLocale.ID
        }

        var customerDetailsToCapture = { email: emailToCapture, language: currentLocaleID };

        var backInStockData = customObjectMgr.getCustomObject(constants.BackInStock_CONFIGURATION_OBJECT, productToCapture)

        Transaction.wrap(function () {
            if (!backInStockData) {
                var backInStockObj = customObjectMgr.createCustomObject(constants.BackInStock_CONFIGURATION_OBJECT, productToCapture);
                backInStockObj.custom.IsMailSent = false;
                backInStockObj.custom.EmailID = new Array(JSON.stringify(customerDetailsToCapture));
            }
            else {
                var existigEmailIdList = backInStockData.custom.EmailID;
                var isCapturedEmailAlreadyexist = existigEmailIdList.find(x => JSON.parse(x).email === emailToCapture);
                if (!isCapturedEmailAlreadyexist) {
                    backInStockData.custom.EmailID = backInStockData.custom.EmailID.concat(JSON.stringify(customerDetailsToCapture));
                }
            }

            logger.info('Data captured successfully.');
        });

        res.json({
            message: Resource.msg('message.confirm.subscription', 'common', null)
        });
        return next();
    });

module.exports = server.exports();