'use strict';

/**
 * @namespace BackInStock
 */

var server = require('server');
server.extend(module.superModule);

/**
 * Render the report page.
 * @name module:controllers/BackInStock~ReportShow
 * @function
 * @memberof BackInStock
 * @inner
 * @param {dw.web.HttpRequest} req - The HTTP request.
 * @param {dw.web.HttpResponse} res - The HTTP response.
 * @param {Function} next - The next function to be called.
 */
server.get('ReportShow', function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');
    res.render('backInStock/report', {
        formAction: URLUtils.url('BackInStock-ReportData'),
        reportExportURL: URLUtils.url('BackInStock-ExportData')
    });
    next();
});

/**
 * Retrieve report data based on search parameters.
 * @name module:controllers/BackInStock~ReportData
 * @function
 * @memberof BackInStock
 * @inner
 * @param {dw.web.HttpRequest} req - The HTTP request.
 * @param {dw.web.HttpResponse} res - The HTTP response.
 * @param {Function} next - The next function to be called.
 */
server.post('ReportData', function (req, res, next) {
    var reportHelpers = require('~/cartridge/scripts/helpers/reportHelpers');
    var searchParameterObject = {
        fromDateSearchParameter: req.httpParameterMap.get('fromDate').stringValue,
        toDateSearchParameter: req.httpParameterMap.get('toDate').stringValue,
        pageIDSearchParameter: req.httpParameterMap.get('pageId').stringValue
    };
    var backInStock = new reportHelpers.BackInStockReport();
    var data = backInStock.getReportData(searchParameterObject);
    data.searchParameters = searchParameterObject;
    res.json(data);
    next();
});

/**
 * Export report data based on search parameters.
 * @name module:controllers/BackInStock~ExportData
 * @function
 * @memberof BackInStock
 * @inner
 * @param {dw.web.HttpRequest} req - The HTTP request.
 * @param {dw.web.HttpResponse} res - The HTTP response.
 * @param {Function} next - The next function to be called.
 */
server.post('ExportData', function (req, res, next) {
    var reportHelpers = require('~/cartridge/scripts/helpers/reportHelpers');
    var searchParameterObject = {
        fromDateSearchParameter: req.httpParameterMap.get('fromDate').stringValue,
        toDateSearchParameter: req.httpParameterMap.get('toDate').stringValue,
        host: req.host
    };
    var backInStock = new reportHelpers.BackInStockReport();
    var data = backInStock.ExportData(searchParameterObject);
    data.searchParameters = searchParameterObject;
    res.json(data);
    next();
});

module.exports = server.exports();
