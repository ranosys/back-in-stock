jQuery(document).ready(function () {

    jQuery("body").on('click', ".js-updateRecords", function () {
        let fromDate = jQuery('#fromDateIB').val();
        let toDate = jQuery('#toDateIB').val();
        let pageId = jQuery(this).attr('page-id');
        let backInStockReport = new BackInStockReport();
        backInStockReport.updateRecords(fromDate, toDate, pageId);
    })

    jQuery("body").on('click', "#reportFilterBtn", function () {
        let fromDate = jQuery('#fromDateIB').val();
        let toDate = jQuery('#toDateIB').val();
        let backInStockReport = new BackInStockReport();
        backInStockReport.updateRecords(fromDate, toDate, 1);
        jQuery(this).hide();
        jQuery("#reportFilterRemoveBtn").show();
    })

    jQuery("body").on('click', "#reportFilterRemoveBtn", function () {
        let backInStockReport = new BackInStockReport();
        backInStockReport.initilize();
        jQuery(this).hide();
        jQuery("#reportFilterBtn").show();
    })

    jQuery("body").on('click', "#reportExportBtn", function () {
        let fromDate = jQuery('#fromDateIB').val();
        let toDate = jQuery('#toDateIB').val();
        let backInStockReport = new BackInStockReport();
        backInStockReport.exportRecords(fromDate, toDate);
    })


    let BackInStockReport = function () {

        this.initilize = function () {
            var yesterdaysDate = dateformat(new Date(new Date().setDate(new Date().getDate() - 1)));
            var todaysDate = dateformat(new Date());
            jQuery("#toDateIB").attr('max',dateformat(new Date()));
            jQuery("#fromDateIB").attr('max',dateformat(new Date()));
            jQuery('#fromDateIB').val(yesterdaysDate);
            jQuery('#toDateIB').val(todaysDate);
            getReportData(yesterdaysDate, todaysDate, 1);
        }

        this.updateRecords = function (fromDate, ToDate, pageId) {
            getReportData(fromDate, ToDate, pageId);           
        }

        this.exportRecords = function (fromDate, ToDate) {
            jQuery.ajax({
                url: jQuery('#reportExportBtn').data("url"),
                type: 'POST',
                data: { fromDate: fromDate, toDate: ToDate },
                success: function (response) {
                    if (response.filePath) {
                        window.location.href = response.filePath;
                        jQuery(".filterMsg").html('');
                    }
                    else {
                        if (response.msg) {
                            jQuery(".filterMsg").html(response.msg);
                            jQuery('.pagination').empty();
                            jQuery('#reportTable > tbody').empty();
                        }
                    }
                },
                error: function (xhr, status, error) {
                    // Handle error
                    console.error(error);
                }
            });
        }

        let getReportData = function (fromDate, ToDate, pageId) {

            jQuery.ajax({
                url: jQuery('#backInStockForm')[0].action,
                type: 'POST',
                data: { fromDate: fromDate, toDate: ToDate, pageId: pageId },
                success: function (response) {
                    if (Object.keys(response.data).length > 0) {
                        jQuery('.pagination').empty();
                        jQuery('#reportTable > tbody').empty();
                        BindRecordsToTable(jQuery('#reportTable > tbody'), response.data);
                        bindPagination(jQuery('.pagination'), response.totalPages);
                        jQuery(".filterMsg").html('');
                    }
                    else {
                        if (response.msg) {
                            jQuery(".filterMsg").html(response.msg);
                            jQuery('.pagination').empty();
                            jQuery('#reportTable > tbody').empty();
                        }
                    }
                },
                error: function (xhr, status, error) {
                    // Handle error
                    console.error(error);
                }
            });
        }

        let bindPagination = function (paginationBtnParentElement, pageLength) {
            for (var i = 1; i <= pageLength; i++) {
                paginationBtnParentElement.append('<button class="page-btn js-updateRecords" page-id=' + i + '>' + i + '</button>');
            }
        }

        let BindRecordsToTable = function (tableBodyElement, records) {
            var productIdList = Object.keys(records);
            for (var i = 0; i < productIdList.length; i++) {
                let customers = records[productIdList[i]].customers; 
                let customersArray = [];
                for(var j = 0; j < customers.length ; j++){
                    customersArray.push(JSON.parse(customers[j]).email);
                }
                tableBodyElement.append('<tr>' +
                    '<td>' + (i + 1) + '</td>' +
                    '<td>' + productIdList[i] + '</td>' +
                    '<td>' + customersArray.join(', ') + '</td>' +
                    '<td>' + customersArray.length + '</td>' +
                    '<td>' + records[productIdList[i]].pendingEmails.length + '</td>' +
                    '</tr>');
            }
        }

        let dateformat = function (date) {
            // Extract the year, month, and day components from the date object
            var year = date.getFullYear();
            var month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-based
            var day = String(date.getDate()).padStart(2, '0');

            // Construct the date string in the format 'YYYY-MM-DD'
            return year + '-' + month + '-' + day;
        }
    }

    let backInStockReport = new BackInStockReport();
    backInStockReport.initilize();

});