
var Logger = require('dw/system/Logger');

/**
 * Represents a BackInStockReport object.
 * @constructor
 */
function BackInStockReport() {
    var FileReader = require('dw/io/FileReader');
    var CSVStreamReader = require('dw/io/CSVStreamReader');
    var FileWriter = require('dw/io/FileWriter');
    var CSVStreamWriter = require('dw/io/CSVStreamWriter');
    var Site = require('dw/system/Site');
    var File = require('dw/io/File');
    var Resource = require('dw/web/Resource');
    var logger = Logger.getLogger('BackInStock', 'BackInStock');
    var dir = File.IMPEX + Site.current.getCustomPreferenceValue('BackInStockNotifications_ReportLocation');
    var tempDir = File.TEMP + File.SEPARATOR + 'backInStockReportHelper' + File.SEPARATOR;

    var listSize = 2;

    var JSONFormatteReportData = {};

    /**
     * Gets the report data based on search parameters.
     * @param {Object} searchParameters - The search parameters.
     * @returns {Object} The report data and total pages.
     */
    this.getReportData = function (searchParameters) {
        var files;
        
        var data;
        var JSONFile;
        var fileRdr;

        var tempJsonFileName = searchParameters.fromDateSearchParameter + '_' + searchParameters.toDateSearchParameter
        var totalPages = 0;

        function applyFilter(file) {
            // Format: yyyyMMdd  
            var fileName = file.getName();
            var fileData = fileName.split('_');
            var fromDate = fileData[0];
            var toDate = fileData[1];
            var pageID = fileName.split('_')[2].split('.')[0];
            var IsDateValid = (new Date(fromDate).getTime() == new Date(searchParameters.fromDateSearchParameter).getTime() && new Date(toDate).getTime() == new Date(searchParameters.toDateSearchParameter).getTime());
            if (IsDateValid) {
                totalPages++;
            }
            return (IsDateValid && pageID == searchParameters.pageIDSearchParameter);
        }
        var IsFileAlreadyExist = new File(tempDir).listFiles(applyFilter);

        if (searchParameters.pageIDSearchParameter == 1) {
            if (IsFileAlreadyExist.length > 0) {
                if (new Date(searchParameters.fromDateSearchParameter).getTime() == new Date().setHours(0,0,0,0) || new Date(searchParameters.toDateSearchParameter).getTime() == new Date().setHours(0,0,0,0)) {

                    for (var m = 0; m < IsFileAlreadyExist.length; m++) {
                        IsFileAlreadyExist[m].remove();
                    }
                    files = getCSVFilesList(searchParameters);
                    if (files.length > 0 && JSONFormatteReportData) {
                        JSONFile = createTempDataFile(tempJsonFileName);
                        fileRdr = FileReader(JSONFile.firstPage);
                        totalPages = JSONFile.totalPages;
                    }
                    else {
                        logger.info(`No CSV files to display ${tempJsonFileName}: getReportData()`);
                        return { data: {}, totalPages: 0, msg: Resource.msg('back.in.stock.report.table.no.records.to.display.msg', 'backInStockReport', null) };
                    }
                }
                else{
                    fileRdr = FileReader(IsFileAlreadyExist[0]);
                }
            }
            else {
                files = getCSVFilesList(searchParameters);
                if (files.length > 0 && JSONFormatteReportData) {
                    JSONFile = createTempDataFile(tempJsonFileName);
                    fileRdr = FileReader(JSONFile.firstPage);
                    totalPages = JSONFile.totalPages;
                }
                else {
                    logger.info(`No CSV files to display ${tempJsonFileName}: getReportData()`);
                    return { data: {}, totalPages: 0, msg: Resource.msg('back.in.stock.report.table.no.records.to.display.msg', 'backInStockReport', null) };
                }
            }
        }
        else {
            if (IsFileAlreadyExist.length > 0) {
                fileRdr = FileReader(IsFileAlreadyExist[0]);
            }
        }

        data = JSON.parse(fileRdr.readString());
        fileRdr.close();
        return { data: data, totalPages: totalPages };
    }

    /**
     * Exports the report data based on search parameters.
     * @param {Object} searchParameters - The search parameters.
     * @returns {Object} The file path of the exported data.
     */
    this.ExportData = function (searchParameters) {
        var tempReportExportDir = File.TEMP + File.SEPARATOR + 'backInStockReport' + File.SEPARATOR;

        var localeDir = new File(tempReportExportDir);
        if (!localeDir.exists()) {
            localeDir.mkdirs();
        }
        var fileName = searchParameters.fromDateSearchParameter + '_' + searchParameters.toDateSearchParameter;
        var CSVFile = new File(localeDir.getFullPath() + File.SEPARATOR + fileName + '.csv');
        var fileWtr = FileWriter(CSVFile);
        var csvWriter = new CSVStreamWriter(fileWtr);
        var headers = ['S_No', 'Product-IDs', 'Customers-Email-IDs', 'Total-Mails-Sent', 'Pending-Mails'];
        csvWriter.writeNext(headers);

        function applyFilter(file) {
            // Format: yyyyMMdd  
            var fileName = file.getName();
            var fileData = fileName.split('_');
            var fromDate = fileData[0];
            var toDate = fileData[1];
            return (new Date(fromDate).getTime() == new Date(searchParameters.fromDateSearchParameter).getTime() && new Date(toDate).getTime() == new Date(searchParameters.toDateSearchParameter).getTime());
        }
        var FileList = new File(tempDir).listFiles(applyFilter);
        var index = 1;
        if (FileList) {
            for (var i = 0; i < FileList.length; i++) {
                var fileRdr = FileReader(FileList[i]);
                var data = JSON.parse(fileRdr.readString());
                fileRdr.close();

                var objectKeys = Object.keys(data);
                for (var j = 0; j < objectKeys.length; j++) {
                    var values = [
                        index++,
                        objectKeys[j],
                        data[objectKeys[j]].customers.join('&##'),
                        data[objectKeys[j]].customers.length,
                        getPendingCapturedData(objectKeys[j]).length
                    ];
                    csvWriter.writeNext(values)
                }
                index++
            }
            csvWriter.close();
            return { filePath: `https://${searchParameters.host}/on/demandware.servlet/webdav/Sites/${CSVFile.getFullPath()}` };
        }
        else {
            logger.info(`No CSV files to display ${fileName}: ExportData()`);
            return { msg: Resource.msg('back.in.stock.report.table.no.records.to.display.msg', 'backInStockReport', null) };
        }
    }

    /**
     * Creates a temporary data file.
     * @param {string} fileName - The file name.
     * @returns {Object} The first page of the file and total pages.
     */
    const createTempDataFile = function (fileName) {
        var localeDir = new File(tempDir);
        if (!localeDir.exists()) {
            localeDir.mkdirs();
        }

        const chunkSize = listSize;
        const result = breakJSONObject(JSONFormatteReportData, chunkSize);

        var fileLength = Math.round(Object.keys(JSONFormatteReportData).length / listSize);

        var pageFirst;
        for (var i = 0; i < fileLength; i++) {
            var JsonFile = new File(localeDir.getFullPath() + File.SEPARATOR + fileName + '_' + (i + 1) + '.json');
            var fileWtr = FileWriter(JsonFile);
            fileWtr.writeLine(JSON.stringify(result[i]));
            fileWtr.close();
            if (i == 0) { pageFirst = JsonFile };
        }
        return { firstPage: pageFirst, totalPages: fileLength };
    }

    /**
     * Breaks a JSON object into chunks.
     * @param {Object} jsonObj - The JSON object.
     * @param {number} chunkSize - The size of each chunk.
     * @returns {Array} The array of chunks.
     */
    const breakJSONObject = function (jsonObj, chunkSize) {
        var keys = Object.keys(jsonObj);
        var result = [];

        for (var i = 0; i < keys.length; i += chunkSize) {
            var chunkObj = {};
            var chunkKeys = keys.slice(i, i + chunkSize);

            chunkKeys.forEach(key => {
                chunkObj[key] = jsonObj[key];
            });

            result.push(chunkObj);
        }

        return result;
    }

    /**
     * Gets the list of CSV files based on search parameters.
     * @param {Object} searchParameters - The search parameters.
     * @returns {Array} The list of CSV files.
     */
    const getCSVFilesList = function (searchParameters) {
        function applyFilter(CSVFile) {
            // Format: yyyyMMdd  
            var fileName = CSVFile.getName();
            var fileCreatedDate = fileName.split('-')[1];
            var year = fileCreatedDate.slice(0, 4);
            var month = fileCreatedDate.slice(4, 6);
            var date = fileCreatedDate.slice(6, 8);
            var formattedFileCreatedDate = new Date(`${year}-${month}-${date}`)
            // Check filters           
            var isVerified = (formattedFileCreatedDate >= new Date(searchParameters.fromDateSearchParameter)) && (formattedFileCreatedDate <= new Date(searchParameters.toDateSearchParameter));
            if (isVerified) {
                appendJSONFormattedData(CSVFile);
            }
            return isVerified;
        }

        var list = new File(dir).listFiles(applyFilter);

        return list;
    }

    /**
     * Appends JSON-formatted data from CSV file.
     * @param {Object} CSVFile - The CSV file.
     * @returns {Object} The appended data.
     */
    const appendJSONFormattedData = function (CSVFile) {
        var fileReader = new FileReader(CSVFile);
        var SteamReader = CSVStreamReader(fileReader);
        var data = SteamReader.readAll();

        for (var i = 1; i < data.length; i++) {
            var customers = data[i][1].split('&##');

            if (Object.prototype.hasOwnProperty.call(JSONFormatteReportData ,data[i][2])) {
                var oldCustomers = JSONFormatteReportData[data[i][2]].customers;
                var allCustomers = customers.concat(oldCustomers);
                var uniqueSet = new Set(allCustomers);
                var uniqueCustomers = Array.from(uniqueSet);
                JSONFormatteReportData[data[i][2]].customers = uniqueCustomers;
            } else {
                JSONFormatteReportData[data[i][2]] = { customers: Array.from(new Set(customers)), product: data[i][2], pendingEmails: getPendingCapturedData(data[i][2]) };
            }
        }
        return JSONFormatteReportData;
    }

    let getPendingCapturedData = function (productID) {
        var customObjectMgr = require('dw/object/CustomObjectMgr');
        var constants = require('*/cartridge/scripts/utils/constants');

        var backInStockData = customObjectMgr.getCustomObject(constants.BackInStock_CONFIGURATION_OBJECT, productID)
        if (backInStockData) {
            return backInStockData.custom.EmailID;
        }

        return [];

    }

}

exports.BackInStockReport = BackInStockReport;