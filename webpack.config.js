var path = require('path');
    var MiniCssExtractPlugin = require('mini-css-extract-plugin');
    var CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
    var RemoveEmptyScriptsPlugin = require('webpack-remove-empty-scripts');
    var sgmfScripts = require('sgmf-scripts');

    const shell = require('shelljs');
    const cwd = process.cwd();

    function createComponentJsPath() {
        const result = {};    
        const directoryList = [
            shell.ls(path.join(cwd, './cartridges/bm_backinstock/cartridge/client/**/js/*.js'))
        ];    
        directoryList.forEach(fileList => {
            fileList.forEach(filePath => {
                let location = path.relative(path.join(cwd, './cartridges/bm_backinstock/cartridge/client'), filePath);
                location = location.substr(0, location.length - 3);
                result[location] = filePath;
            });
        });    
        return result;
    }   
    
    function createComponentCssPath() {
        const result = {};    
        const directoryList = [
            shell.ls(path.join(cwd, './cartridges/bm_backinstock/cartridge/client/**/scss/*.scss'))           
        ];    
        directoryList.forEach(fileList => {
            fileList.forEach(filePath => {
                let location = path.relative(path.join(cwd, './cartridges/bm_backinstock/cartridge/client'), filePath);
                location = location.substr(0, location.length - 5);
                var locationSplit = location.split('\\');
                locationSplit[1] = 'css';
                location = locationSplit.join('\\');
                result[location] = filePath;
            });
        });    
        return result;
    }   

    module.exports = [    
    {
        mode: 'production',
        name: 'js',
        entry: Object.assign({},  sgmfScripts.createJsPath(), createComponentJsPath()),
        output: {
            path: path.resolve('./cartridges/plugin_backinstock/cartridge/static'),
            filename: '[name].js'
        }
    },  
    {
        mode: 'none',
        name: 'scss',
        entry: Object.assign({},  sgmfScripts.createScssPath(), createComponentCssPath()),
        output: {
            path: path.resolve('./cartridges/plugin_backinstock/cartridge/static')
        },
        plugins: [
            new RemoveEmptyScriptsPlugin(),
            new MiniCssExtractPlugin({
                filename: '[name].css',
                chunkFilename: '[name].css'
            })
        ],
        module: {
            rules: [
                {
                    test: /.scss$/,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                            options: {
                                esModule: false
                            }
                        },
                        {
                            loader: 'css-loader',
                            options: {
                                url: false
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                postcssOptions : {
                                    plugins: [require('autoprefixer')]
                                }
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                implementation: require('sass'),
                                sassOptions: {
                                    includePaths: [
                                        path.resolve('node_modules'),
                                        path.resolve(
                                            'node_modules/flag-icon-css/sass'
                                        )
                                    ]
                                }
                            }
                        }
                    ]
                }
            ]
        },
        optimization: {
            minimizer: ['...', new CssMinimizerPlugin()]
        }
    }];